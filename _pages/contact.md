---
title: Contact
subtitle: Send me an email, I'd love to hear from you!
description: Personal is the perfect theme for developers, designers and other creatives.
featured_image: /images/pages/contact/contact2.jpg
---

{% include contact-form.html %}

<!-- We've made a contact form that you can use with [Formspree](https://formspree.io/create/jekyllthemes) to handle up to 50 submissions per month for free. You could also easily switch out the end-point to use another contact form service. -->
