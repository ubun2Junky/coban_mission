---
title: Guatemala
subtitle:
description:
featured_image: /images/pages/guatemala/tikal_canopy_2.jpg
---
**Guatemala**, country of Central America. The dominance of an Indian culture within its interior uplands distinguishes Guatemala from its Central American neighbours. The origin of the name Guatemala is Indian, but its derivation and meaning are undetermined. Some hold that the original form was Quauhtemallan (indicating an Aztec rather than a Mayan origin), meaning “land of trees,” and others hold that it is derived from Guhatezmalha, meaning “mountain of vomiting water”— referring no doubt to such volcanic eruptions as the one that destroyed Santiago de los Caballeros de Guatemala (modern-day Antigua Guatemala), the first permanent Spanish capital of the region’s captaincy general. The country’s contemporary capital, Guatemala City, is a major metropolitan centre; Quetzaltenango in the western highlands is the nucleus of the Indian population. [^1]

**Cobán**, city, north-central Guatemala, situated 4,331 feet (1,320 metres) above sea level in the Chamá Mountains on the Cahabón River. Founded about 1538 near Mayan ruins and named for the Indian chieftain Cobaóu, the city developed as the major urban centre of northern Guatemala. A 17th-century church still stands. Cobán was the centre of a large colony of German settlers who went to Guatemala in the late 19th century to grow coffee. Most of these landowners were deported during World War II by the Guatemalan government (under pressure from the United States) for supporting the Nazi Party. Las Victorias National Park, just south of Cobán, is on the site of a former German-owned coffee plantation, as is Verapaz Nursery (1898), which preserves orchids endangered by logging operations in the area. The agricultural hinterland is best known for its coffee, but tea, cacao (the source of cocoa beans), vanilla, spices, grains, livestock, and hardwoods are also significant. The city was formerly an important commercial and manufacturing centre. Much of the agricultural produce now bypasses Cobán and goes directly to Guatemala City, 130 miles (210 km) south. An annual folklore festival, which draws many tourists, takes place in Cobán in the last week of July or first week of August. Pop. (2002) 47,202.[^2]
<div class="gallery" data-columns="3">
    <img src="/images/pages/guatemala/Guatemala-World-Map.png">
    <img src="/images/pages/guatemala/guatemala-map.gif">
    <img src="/images/pages/guatemala/IMG_0973.JPG">
</div>

<div class="gallery" data-columns="1">
    <img src="/images/pages/guatemala/slideshow/flag_1.jpg">
    <img src="/images/pages/guatemala/slideshow/flores_1.jpg">
    <img src="/images/pages/guatemala/slideshow/market.jpg">
    <img src="/images/pages/guatemala/slideshow/rio_dulce_1.jpg">
    <img src="/images/pages/guatemala/slideshow/rio_dulce_2.jpg">
    <img src="/images/pages/guatemala/slideshow/semuc_champay_4.jpg">
    <img src="/images/pages/guatemala/slideshow/semuc_champay_1.jpg">
    <img src="/images/pages/guatemala/slideshow/semuc_champay_2.jpg">
    <img src="/images/pages/guatemala/slideshow/semuc_champay_3.jpg">
    <img src="/images/pages/guatemala/slideshow/tikal_1.jpg">
    <img src="/images/pages/guatemala/slideshow/tikal_2.jpg">
    <img src="/images/pages/guatemala/slideshow/tikal_3.jpg">
    <img src="/images/pages/guatemala/slideshow/tikal_4.jpg">
    <img src="/images/pages/guatemala/slideshow/tikal_canopy_1.jpg">
    <img src="/images/pages/guatemala/slideshow/tikal_canopy_2.jpg">
</div>

[^1]:Britannica (https://www.britannica.com/place/Guatemala)
[^2]:Britannica (https://www.britannica.com/place/Coban)
