---
title: About me
subtitle:
description:
featured_image: /images/pages/about/about.jpg
---
> I am a member of the church of Jesus Christ of Latter-Day Saints. I love this gospel and I know that it is true! I know that Jesus lives and through prayer, we can come closer to him. I know that he loves his children with all his heart, and wants us to come unto him. I know that we can do this through praying, reading the Book of Mormon, and having faith in him. I have read The Book of Mormon and I know it is true. I know that Joseph Smith was a prophet and translated The Book of Mormon through the Holy Ghost.
I know that he knows and understands each of his children. I know Jesus Christ has sacrificed and atoned for all sins of the world. I know that through him, we can ask for forgiveness, and be made clean again. I know that in his plan, families can be made eternal. I love his plan with all my heart. I am so grateful for the opportunity to serve him and help others know what his love looks and feels like. This is my testimony in the name of Jesus Christ, amen.

<div class="gallery" data-columns="3">
    <img src="">
    <img src="/images/pages/about/DSC_8526-Edit.jpg">
    <img src="">
</div>
