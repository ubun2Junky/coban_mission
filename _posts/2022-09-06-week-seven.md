---
title: "Week 7"
subtitle: 'September 6, 2022'
date: 2022-09-06 00:00:00 -700
description: "Bebé en San Chapín!"
featured_image: '/images/posts/09-06-2022/image_12_09-06-2022.jpg'
---
![](/images/posts/09-06-2022/image_12_09-06-2022.jpg)

It's crazy how much it feels like time has passed! The days are long but boy are the weeks short! I'll be hitting my two month period next week.

Anyways, I'm sure all of you are wondering where I am, who I'm with, and if I'm surviving or not. I'll tell you after i talk about the CCM!

Saying goodbye to the district, my Hermana, and our teachers at the CCM was probably the hardest thing i was asked to do. Yes, leaving the family was hard but leaving the CCM was harder. My family i know I'll see again in a year and a half, but some of the friendships I made at the CCM were there to last only four weeks.

I constantly miss being surrounded by people who could bring the spirit. I didn't realize how much power and spirit an MTC holds until i left.

Buuuuttt, when i did leave at 3 a.m... i was automatically slapped in the face with Spanish and Spanish only. If you've ever been in a place where light skin and green eyes is a minority, you know what's its like walking down the streets of Guatemala. Things of course are completly different.

Drumroll please.....Immmmm.....Innnnn....San Jerónimo!! Yayyyy!

It's like the West Haven of Guatemala. Its around 20 mins away from the city Salama. And around 2 hours away from Cobán. It's so so so beautiful. And what I mean by West Haven is lots of cows and crops.

The people here are amazing, humble, and kind. And so is my companion, Hermana Morales. She's so so patient with me and kind during lessons when i struggle to communicate with a lot of people.

Anyways, more will be explained next week but i gotta go. Thanks for reading, love you so much!

<!-- Fotos:
1. Distrito 8A!
2. San Jeronimo Baja Verapaz
3. Hermana Morales and I!
4.  -->
<div class="gallery" data-columns="2">
    <img src="/images/posts/09-06-2022/image_01_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_02_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_03_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_04_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_05_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_06_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_07_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_08_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_09_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_10_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_11_09-06-2022.jpg">
    <img src="/images/posts/09-06-2022/image_12_09-06-2022.jpg">
</div>
