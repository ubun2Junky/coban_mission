---
title: "The best is yet to come."
subtitle: 'July 17, 2022'
date: 2022-07-17 00:00:00 -700
description: 'Buenas días amigos! I am so happy! Today was my setting apart for my mission.'
featured_image: '/images/posts/7-17-2022/IMG_3424.JPG'
---
![](/images/posts/7-17-2022/IMG_3424.JPG)

Buenas días amigos! I am so happy! Today was my setting apart for my mission. I am now officially a missionary, badge and everything! My schedule went from eating breakfast at 10 en la manana, and then cleaning, working, dinner, watching 3 episodes of a kdrama, and finally getting to bed at 12, to waking up at 5:30, eating breakfast, exercising, getting ready, going to class with up to 30 mins of a break in between. This goes on till my last class, which ends at 6:30. Not even my online COVID classes were this busy! However, I am grateful for this opportunity I am given with the support and love from my family and friends. Thank you again everyone! I'll make sure to catch you all up every Thursday! Have a great semana! Talk to you soon!
Los Fotos:
1. My family and I are celebrating my setting apart together! This was taken after eating delicious tacos de pescado y pastel.
2. Officially Hermana Possie' now!!

<div class="gallery" data-columns="2">
    <img src="/images/posts/7-17-2022/IMG_3417.JPG">
    <img src="/images/posts/7-17-2022/IMG_3424.JPG">
</div>
