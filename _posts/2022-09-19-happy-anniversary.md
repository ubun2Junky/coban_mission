---
title: "Week 9"
subtitle: 'September 19, 2022'
date: 2022-09-19 00:00:00 -700
description: "Happy Anniversary!!"
featured_image: '/images/posts/09-19-2022/image_12_09-19-2022.jpg'
---
![](/images/posts/09-19-2022/image_12_09-19-2022.jpg)

Hello everyone! I've loved reading all of your comments on the blog! I miss you guys too!

As normal, crazy week. If you didn't know this week was Guatemala's national independence week. So lots of marimbas, dancing, music, and food. However, don't worry my companion and I didn't attend the parties. It's a good thing because it means that we worked hard with many of the members and investigators.

We visited with many of the members this week. I'm finally starting to get names and places down. It's confusing sometimes when you're in the city that apparently goes by various names. Weird.

Okay, weirdest thing I ate by far was.... pig foot. Just thinking about it.....no, i cant think about it.

This coming week is going to be an interesting one. My companion is going to the capital for 3 days. That means that I get a companion who has never been in my area before. Which means... I'm going to learn a lot this week!  

I know these emails are getting shorter and shorter but I was just informed that the Internet is going out soon. So I'll talk to you next week. Love you all. Have a good week!

<iframe width="1180" height="1180" src="https://www.youtube.com/embed/h0776vCuhxo" title="San Jeronimo Guatemala Marimba" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<p style="text-align: center;">Native instrument of Guatemala the marimba.</p>

Fotos:
1. Hermano Nino and his daughter Brianna.
2. POLLO CAMPERO! YEAHHH BABY!
3. Went to eat pinol con Hermana Blanca!
4. Mi mochila.
5. Feliz 4 cumplemes Hna. Morales (4 months anniversary)
6. Flour tortillas her favorite
7. Mi 2 cumplemes! (2 month anniversary)
8. Feria!!
9. Marimba players
10. 201 Guatemalan anniversary
11. Las Hermanas
12. Las Hermanas
13. Street volleyball!

<div class="gallery" data-columns="2">
    <img src="/images/posts/09-19-2022/image_01_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_02_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_03_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_04_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_05_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_06_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_07_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_08_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_09_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_10_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_11_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_12_09-19-2022.jpg">
    <img src="/images/posts/09-19-2022/image_13_09-19-2022.jpg">
</div>
