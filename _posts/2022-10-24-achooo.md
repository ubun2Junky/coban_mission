---
title: "Week 14"
subtitle: 'October 24, 2022'
date: 2022-10-24 00:00:00 -700
description: "Achoooo...."
featured_image: 'images/posts/10-24-2022/image_04_10-24-2022.jpg'
---
![](/images/posts/10-24-2022/image_04_10-24-2022.jpg)

Hola hola hola!!

Hi everyone, I'm sorry this came in really late, I have no excuse!  This week not much has happened.  We started our week off with lots of rain and no jacket!  Sorry mom, you can never tell though!  Some mornings start really hot and then around noon, it starts to pour!!  Normally I carry around a poncho with me, but these first couple of days I forgot and now I'm paying the price.

Monday I started to feel sick and after being out in the cold of course I got a little worse.  Anyways mid-week I was in bed for the majority of the day.  I think I've gotten more sleep that day than any other day on my mission.  I took loads of cold medication and drank maybe 8 cups of tea a day.  Thanks to my lovely companion, I had chicken soup and finished that day off with more sleep.

The next morning I was feeling somewhat okay, but before we planned to leave, Hna. Morales took my temp and of course I had a fever.  So for the next couple of days I stayed home.  We finished lots and lots of personal study and companion study.  By the end of the week I was feeling ready to get back out and server again with the help of Hermana Alvarado and the mission nurse.

This coming week we'll be spending some time in Cobán for the multizone meeting.  I really love the multizones they hold here.  I like vising with some of the other sister and elders I'm friends with.

But that's basically all that happened this week.  Sorry it's short but I basically slept all this week!  Thank you guys for reading and being patient with me!  Love you lots, talk to you guys this coming Monday!


Fotos:
1.  Getting home sick and attempting to make corn dogs!!
2.  My new buddy!
3.  My flu meds!
4.  Hermanas
5.  Waiting for the bus
6.  Hermanas

<div class="gallery" data-columns="2">
    <img src="/images/posts/10-24-2022/image_01_10-24-2022.jpg">
    <img src="/images/posts/10-24-2022/image_02_10-24-2022.jpg">
    <img src="/images/posts/10-24-2022/image_03_10-24-2022.jpg">
    <img src="/images/posts/10-24-2022/image_04_10-24-2022.jpg">
    <img src="/images/posts/10-24-2022/image_05_10-24-2022.jpg">
    <img src="/images/posts/10-24-2022/image_06_10-24-2022.jpg">
  </div>
