---
title: "Week 17"
subtitle: 'November 14, 2022'
date: 2022-11-14 00:00:00 -700
description: "Arinconadas...."
featured_image: 'images/posts/11-14-2022/image_07_11-14-2022.jpg'
---
![](/images/posts/11-14-2022/image_07_11-14-2022.jpg)

HOLA HOLA HOLA! Buenas tardes everyone! I'm actually writing on time today because we decided to relax a little more this p-day! This week was an amazing week, and were thinking this is the last week together as companions since we have transfers this coming Saturday. We'll see how that goes, you never know with transfers!

We started the week off in Cubulco, since the bus ride from Cubulco to San Jeronimo is around 3 hours long we only do one trip a day. So we spent Monday in Cubulco hiking and hanging out with our district and a couple of the members in Cubulco, and then Tuesday we hit the road once again after a couple of meetings. Took our 3 hour bus ride, which is actually 3 different buses, to our home. It's always nice to lay down for literally seconds and relax just a bit after traveling.  If you don't normally get sick traveling, try going on one of these buses then you'll know what it feels like.

We spent some time exercising this week! On a normal day we spend maybe 3 hours in total walking, however this week was a little different. This Friday we went to the Arinconadas to visit a member's house for lunch. We had never been to her house but I convinced my companion that it wouldn't be as bad as everyone said it would be. Well we started walking around 830 and actually got to her house at 11. My companion almost passed out a couple of times, but gracias a Dios we had a lot of shade and I brought agua pura with me! We finally made it, and were welcomed with once again, a chicken to kill. So after not killing (because we were exhausted) but watching one of the members kill the chicken we ate a hardy chicken soup. Which really made us super sleepy. We helped a member make candle vases for this coming Christmas! It was so much fun! She learned how to make it in an art class she takes in Salama. So we got to learn a little something from her! Although the walk was super long it was well worth it, some of their cousins joined us during the lesson. We taught about faith and the power of prayer. They loved the lesson so much that they told their cousin the day after that they want to prepare for baptism!!! YES! We'll be very familiar with that area this coming week and the weeks after.

The rest of this week we went people hunting. Looking for fresh meat to teach. We walked for more than 5 hours that day. Mostly because we got the impression to travel to an area we weren't very familiar with and totally found a lot of people to teach, but got lost on the way out. Hahaha

Earlier this week we were visited by Presidente Alvarado and his wife, Hermana Alvarado, to do interviews and check to see if our house was clean. Yes, it is a problem apparently here on the mission. Thankfully she said that our house was spotless. That's no surprise though, after all of my childhood being trained by mom and dad to keep things clean when people visit I knew just how to do it. I noticed this week instead of saying "is it dad clean" I found myself saying "is it Hermana Alvarado clean". Good times.

Well, that's the majority of this week! We'll be traveling again tomorrow morning, this time to Coban for a zone meeting. It's weird to see and hear everyone getting ready for thanksgiving, because here....nada. We jump right into Christmas, which is also weird because everything is green and sunny and there is no deck the halls on the radio, but we'll see when it gets closer to time.

Anyways, I love you guys so so much! I'm grateful for everything you all have done for me and I'm grateful for your love and support! Thank you for reading!
Love you all!

Arianna

Fotos:
1. What it looks like before our hike to Arinconadas
2. Rocky road is tough on the feet.
3. Hna. Carolina making vases for candles.
4. Lunch time!!
5. Our vases.
6. My vase!
7. Hna. Angely and I
8. Sometimes nothing beat a good old fashioned burger and fries!

<div class="gallery" data-columns="2">
    <img src="/images/posts/11-14-2022/image_01_11-14-2022.jpg">
    <img src="/images/posts/11-14-2022/image_02_11-14-2022.jpg">
    <img src="/images/posts/11-14-2022/image_03_11-14-2022.jpg">
    <img src="/images/posts/11-14-2022/image_04_11-14-2022.jpg">
    <img src="/images/posts/11-14-2022/image_05_11-14-2022.jpg">
    <img src="/images/posts/11-14-2022/image_06_11-14-2022.jpg">
    <img src="/images/posts/11-14-2022/image_07_11-14-2022.jpg">
    <img src="/images/posts/11-14-2022/image_08_11-14-2022.jpg">
</div>
