---
title: "Week 13"
subtitle: 'October 17, 2022'
date: 2022-10-17 00:00:00 -700
description: "Our first baptism!!"
featured_image: 'images/posts/10-17-2022/image_06_10-17-2022.jpg'
---
![](/images/posts/10-17-2022/image_06_10-17-2022.jpg)

Hola todos!  I hope everyone had a great week because I did!  It was definitely busy busy, but I'm sure it could get busier.  We started the week off with a baptism on the 15th.  The investigator's name is Angela and she's 9 years old.  She's the cutest!  She has a huge desire to learn and live the gospel.

This past week was transfers, my companion and I are still here in San Jeronimo!  She's awesome, I definitely learn a lot from her and really fast too!

This week we had family home evening with two different families.  Hermana Luz, Hermana Lily and the Reyes family.  They were extremely wonderful lessons, prepared by the families.  It's nice to get a break from teaching as well!

I gave my first English lesson this week!  My comp isn't very interested in learning a new language so it's been Spanish and only Spanish for me from day 1!  Yay for me!  I swear, if I don't come home from the mission fluent in Spanish I'm putting myself back in!  My Spanish has been improving really fast now, I've been taking Spanish classes from an Elder every Thursday and Saturday and it's been helping me a lot.  The only thing I'm struggling with are jokes!  Literally any type of joke.  I'll paint a picture for you...

Normally a member will think of a joke and say it, everyone at the table will laugh, except for one person...  ME.  Sometimes the joke is about me so when people see my reaction they think I'm mad at them, when actually I have no idea what they said, but it's better than fake laughing and them asking if I understood.

OUR WARD MISSION LEADER FIXED OUR WATER HEATER!!  This is the second biggest blessing this week!  My companion and I have had to boil water in order to shower with warm water, but as of late we just toughed it out and showered with cold water.  With the change in weather and things getting cooler, we're very grateful for the water heater!  

Yesterday was exactly my third month on my mission.  So that means today I've completed more than three months.  When I actually feel like I've only completed one month.  Time moves by really really fast!  I'm almost done with my training as a junior missionary!  Scary!  I have 4 more weeks till I'm "ready" to be a trainer.  We'll see.  Many of the Elders are saying I could possibly be a trainer by the next transfer in 6 weeks.  With the amount of Hermanas leaving for home and the amount of new incoming they are going to need many trainers.  8 new Hermanas in November.  That's just crazy!!

That's my report for this week!  I'm so sorry it was late!  I miss you guys and love you guys very much.  I'll share more next week.

Fotos:
1.  Banana bread from Hermana Juliana!
2.  French toast!!
3.  Flooded river after tropical storm Julia
4.  My favorite book
5.  Hna. Morales and I
6.  Angela and us
7.  New site
8.  Baptisimal font
9.  Angela and us during her Baptism
10.  Angela's baptism
11.  Kindergarten school!
12.  Hna. Morales and I

<div class="gallery" data-columns="2">
    <img src="/images/posts/10-17-2022/image_01_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_02_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_03_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_04_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_05_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_06_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_07_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_08_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_09_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_10_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_11_10-17-2022.jpg">
    <img src="/images/posts/10-17-2022/image_12_10-17-2022.jpg">
</div>
