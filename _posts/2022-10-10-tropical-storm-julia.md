---
title: "Week 12"
subtitle: 'October 10, 2022'
date: 2022-10-10 00:00:00 -700
description: "Tropical Storm Julia!!"
featured_image: 'images/posts/10-10-2022/image_01_10-10-2022.jpg'
---
![](/images/posts/10-10-2022/image_01_10-10-2022.jpg)

Buenas Todos!!  It feels like I just wrote my weekly letter yesterday!  Time moves by so fast on a mission.  I was telling my comp yesterday that I feel like the CCM was 2 weeks, when it was actually 6 weeks and my almost 2 months in Guate has felt like only 3 weeks. All in all I feel like I've been gone for only 5 weeks when really I've been out for 3 months (as of October 18th).

This week wasn't as crazy as other weeks have been.  We started in Cobán for another intercambio (exchange).  Hermana Morales took the long trip to the capital with Hermana Mendez while I stayed behind with Hermana Tzoc (pronounced S-U-C)  We had a lot of fun together.  The first day we were together she had all our lessons and appointments planned out, however nothing ever goes to plan.  After hearing "Sorry, I know we said we weren't busy, but we actually are.." for the 7th time we decided to do some contacting.  I never really liked knocking on doors, but you got to do what you have to do.  We knocked on around 18-20 doors and received 5 new people to teach!  Now I'm kind of a pro at contacting.

It was Hermana Tzoc's 'cumplimes' that day (13 month on the mission) so we ended the day by celebrating with crepes.  Those were so good!!  The next day, we went to another meeting and it was almost all in Q'eqchi.  San Jeronimo only has a handful of Q'eqchi speakers, but all missions in Alta Vera Paz and Peten have a majority of Q'eqchi speakers.  Thank goodness there is always a members willing to be a translator.  That day we ended up inviting them (all 3) to get baptized and 2 of them said yes!!  So Hermana Tzoc will be there to celebrate their baptism.

Later that week Hermana Morales and I went home and were immediately warned about tropical storm Julia that was going to affect Guatemala.  For the next 3 days we are going to be sheltering in place until the storm passes by.

Guatemala is entering its 'fall-time' season, so it's starting to get a little colder.  Long sleeve shirts and jacket time!

Anyways this has been my week!  We have our first (my first) baptism this coming Saturday and I'm so excited about that.  We've been weeding through some of our eternal investigators and keeping the ones that are actually interested in the gospel.  We need more new people so we'll have to do more contacting.  Oh yeah, the storm did hit but not nearly as bad as it probably looks on tv.  Just lots and lots of rain.

This month has been the month of the 'flu' and COVID.  My companion and I have been very very careful with washing our hands and making sure the house is clean all the time.  However there is only so much you can do during the rainy season.  So sad to say that my companion caught a cold.  We've been eating lots of warm foods but it's hard to get better when you don't like soup.  Soup fixes everything when you have a cold, right?  We'll see how she feels after a couple of appointments today.  If she's still feeling bad I'd rather stay in and work on getting her better instead of making her worse.  Anyways, this was my week!  The weather is getting colder but it's weird that none of the leaves are changing color yet.  So send me some "fall selfies" when you get a chance!!

I miss you guys and love you, talk to you next week!

<iframe width="560" height="315" src="https://www.youtube.com/embed/g6m-sxZqyZY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
