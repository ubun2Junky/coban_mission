---
title: "Llegada A La Mision"
subtitle: 'August 30, 2022'
date: 2022-08-30 00:00:00 -700
description: "I've finally arrived in Guatemala!!!"
featured_image: '/images/posts/08-30-2022/image_02_08-30-2022.jpg'
---
![](/images/posts/08-30-2022/image_03_08-30-2022.png)

**Misión Guatemala Cobán**  
6 ave 2-36 Zona 1, Cobán, A.V

Dear Possié Family,  
We are very pleased to greet you, and at the same time, inform you that your daughter, Sister Possié arrived safe and sound in the Guatemala Cobán Mission. We have attached a photo of your Missionary with us. We are very happy that your Missionary is now here with us.  

We thank you for preparing her during her life to fulfill her sacred calling of “inviting others to come unto Christ by helping them receive the restored gospel through faith in Jesus Christ and His Atonement, repentance, baptism, receiving the gift of the Holy Ghost, and enduring to the end.”  

We anticipate that during their time as a missionary, she will be FINDING the children of God that are ready to receive the eternal truths, TEACHING the restored Gospel of Jesus Christ, BAPTIZING with the authority of the Priesthood and RETAINING through the support of the Church leaders and the faithful members in the area that they serve. We hope that in your weekly conversations with your missionary that you can feel the joy of the conversion process of the children of God.  

Thank you for sharing Sister Possié with our mission. We will take care of her!!!  

We hope that you can feel the joy in having a representative of the Lord in the mission field.  

With love,  
Presidente and Hermana Alvarado  
Guatemala Cobán Mission

<div class="gallery" data-columns="2">
    <img src="/images/posts/08-30-2022/image_01_08-30-2022.png">
    <img src="/images/posts/08-30-2022/image_02_08-30-2022.jpg">
</div>
