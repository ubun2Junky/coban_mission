---
title: "Week Three & Four"
subtitle: 'August 1, 2022 - August 11, 2022'
date: 2022-08-11 00:00:00 -700
description: "If I had one word to describe this week it would be...."
featured_image: '/images/posts/8-11-2022/image_4-08-11-2022.jpeg'
---
![](/images/posts/8-11-2022/image_4-08-11-2022.jpeg)

If I had one word to describe this week it would be....

Muy muy muy ocupada, oh wait, that was more than one word. ¡Hola todos! It's good to finally get a small break. As I sat down at the computer and thought about what has happened over the past 8 days many many things popped into my head. So much has happened so hopefully it sounds better on this email than it does in my head.

Starting with my flight to Mexico last Tuesday, everything was a blur. Leaving was definitely the hardest thing I've ever put myself through. However, as I walked into security wiped my tears and got everything together, I was off! Many people comforted me as they saw me starting to tear up again and again in line. Honestly they probably thought I looked crazy. Everything after that went well until it was time to get on the bus in Mexico. I won't explain in detail because it was quite embarrassing. I was exiting the airport on the way to the bus and there was a really steep ramp. Long story short, I was carrying about 130 lbs of luggage until it somehow ended up dragging me down the ramp, ultimately slipping out of my hand and knocking the Elder who was in front of me down to the ground. I've never seen such an accurate representation of the "domino effect".

After getting unpacked and settled, things started to fall into place. We were greeted by many kind and wonderful missionaries that had only been here for one or two weeks longer than us.  However for some reason, it seemed like they had been here much longer. When it came to having conversations in Spanish or about the gospel they seemed so cool and smart about it.

We started attending classes my second day there. We've been having such a great time learning and teaching with our class. Our teachers are amazing and their lessons have made me feel like I know more than I have in the past 5 years. Which is probably true.

We received assignments\\callings for our class\\district, and I am so excited to say that I am a Sister Trainer Leader, which if you are familiar with the callings it is equivalent to that of a Zone Leader. So I've been taking advantage of that calling by visiting other Hermanas in our rama (branch). They are so awesome. Seriously, Hermana Williams, Hermana Rincon, Hermana Gonzalez, and Hermana Ventura are amazing women. I am so grateful they sacrificed so much to be able to go on a mission. It's definitely not easy, but it's totally worth it.  I could talk about them all day but my Hermana just finished writing her email so I need to make this last part quick.

I made a new friend, his name is David, he works in the comedor (kitchen). He is amazing! Since my first day at the CCM he has been kind to me and is diligent in cleaning the cafeteria for us missionaries. After a couple of days I decided to write him a thank you note for all that he does for us here at the CCM.  Soon after.... we became best friends. He gave me a strawberry sucker today, so I'm assuming that means we're besties.

Other than that, classes at the CCM have been excellent. My Spanish went from not knowing any words related to the gospel, to teaching about the gospel in 15 mins. In my eyes that's a huge improvement.

I'm excited to hear from every one! Thanks for being so patient with me! I'll be posting again next Thursday so stay tuned! Thank you so much for all of your support and love.  I feel it from all the way over here. Don't worry, I'm thinking about all of you guys everyday! I'll make sure to continue to share my love with every one here at the CCM!
Thank you guys again!

Los quiero mucho!

Las fotos:
1. Some of the Elders in my district.....they look so cool.
2. District selfie!!
3. Our visit to the temple today! Such a wonderful experience!
4. We're too cool...
5. During class. I promise this was during a break.
6. Rama 8 at the temple! Love those girls so so much! Muchisima!
7. Didn't know this girl but we wore the same dress on the same day, and then somehow got seated together at a devotional.

<div class="gallery" data-columns="3">
    <img src="/images/posts/8-11-2022/image_1-08-11-2022.jpeg">
    <img src="/images/posts/8-11-2022/image_2-08-11-2022.jpeg">
    <img src="/images/posts/8-11-2022/image_3-08-11-2022.jpeg">
    <img src="/images/posts/8-11-2022/image_4-08-11-2022.jpeg">
    <img src="/images/posts/8-11-2022/image_5-08-11-2022.jpeg">
    <img src="/images/posts/8-11-2022/image_6-08-11-2022.jpeg">
    <img src="/images/posts/8-11-2022/image_7-08-11-2022.jpg">
</div>
