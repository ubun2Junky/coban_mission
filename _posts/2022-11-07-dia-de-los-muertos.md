---
title: "Week 16"
subtitle: 'November 07, 2022'
date: 2022-11-07 00:00:00 -700
description: "Dia de Los Muertos...."
featured_image: 'images/posts/11-07-2022/image_11_11-07-2022.jpg'
---
![](/images/posts/11-07-2022/image_11_11-07-2022.jpg)

Hola everyone!!  How has everyone been this week?  I'm hoping you all had a fantastic Halloween and a Happy Birthday (Lauren).

This week was a great week!  We started off sick again (well I did anyways).  With a fever of 102F and nausea, but of course after getting enough sleep and actually drinking pure water I'm feeling a lot better now.  I've been put on a strict water only diet and of course not just any water...  agua pura (pure water).  Other than pure water costs more than soda here I have no objections.  Pure water is a little expensive here.

A few days after I got better, we celebrated 'Day of the Dead'.  We contacted and found new people to teach literally in the cemetery.  It went really well.  The Day of the Dead was really interesting here!  It's  a little different to how learned Mexico celebrates the holiday.  Kites are extremely popular!  It's a tradition celebrated during the Day of the Dead.  Families take kids mid-day to fly kites.  It's actually really beautiful seeing all of the colorful kites in the air.  We never got the opportunity to make or fly a kite, but thank goodness kite season isn't over yet!  Another thing that was a little different is a traditional food called Fiambre.  I posted a photo, it's some sort of cold salad with mixed cheeses and cold meats.  It wasn't my favorite trying it for the first time, but now I can say I've had Fiambre on the Day of the Dead.  Another thing I noticed was that only a small group of people actually celebrate the holiday at the cemetery.  Most people are in their homes with their families/friends and others are out drinking.  The decorations were very very beautiful.

The next day we celebrated my companion's 25th birthday!  Putting up decorations and making cards is my specialty now!  I surprised her with a pretty card, decorations and yummy pancakes.  She was very happy!  We also bought a cake and went to many member's and investigator's homes to celebrate.  By the end of the day I really did feel like I was literally going to explode!  We had a fun day with many gifts and lots and lots of cake!

That's how my week went.  Thank you for checking up on me once again!  Make sure to look at the photos!  (How could you miss those, they're the best part!)

Love you guys so so much.

Love Arianna.

Fotos:
1.  Fiambre
2.  Day of the Dead decorations
3.  Day of the Dead decorations
4.  I love sunflowers
5.  James, Hna. Morales and I
6.  Hna. Morales 25th birthday card
7.  Birthday Celebrations
8.  Hna. Maribeth, Me and Hna. Morales
9.  Birthday cake #2!!
10. Hiking to the mirador
11. Group portrait
12. Tags
13. The view from the mirador
14. Peace!

<div class="gallery" data-columns="2">
    <img src="/images/posts/11-07-2022/image_01_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_02_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_03_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_04_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_05_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_06_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_07_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_08_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_09_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_10_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_11_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_12_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_13_11-07-2022.jpg">
    <img src="/images/posts/11-07-2022/image_14_11-07-2022.jpg">
</div>
