---
title: "Week 15"
subtitle: 'October 31, 2022'
date: 2022-10-24 00:00:00 -700
description: "Chicken Soup...."
featured_image: 'images/posts/10-31-2022/image_02_10-31-2022.jpg'
---
![](/images/posts/10-31-2022/image_02_10-31-2022.jpg)

Hola, buenas tardes a todos!  Hi everyone, it's been a crazy crazy awesome week!  After being sick almost all last week, I was definitely ready to leave the house and get teaching again.  We started the week by teaching and preparing for the multizone.

We have multizones, which is basically a meeting with all of the missionaries.  These are held about every 6 weeks, which fly by so so fast!  We went to the multizone this past Wednesday.  It's always nice to see other missionaries and visit with them.  I love hearing their experiences.  I got to hear from some of the Elders (male missionaries) and they shared their experiences from their areas.  I used to complain that we had to walk 3 hours in the day to get to some of the members homes, but after hearing the Elders talk about coming home after walking for 6 hours with blisters in their feet and on their shoulders, I'm not going to complain any longer.  They told stories about their mission that of course made me cry a lot.  That's one thing that's changed since I started my mission.  Whenever anyone shares their testimony I cry.  Don't get me started on going home mission testimonies.  I remember my first meeting, I couldn't stop crying!  Literally all the Elders and some of the Sisters who didn't know me thought there was something wrong with me.  We finished the meeting later that day and then went to the Sister's house in San Marcos.  We talked together about the meeting.  It's always so nice to be with these girls!

We traveled home the next day for 2 hours in the bus with Hna. Muñoz and Hna. Herrera.  Those two are some of the greatest missionaries!  When we got home we got lunch, ate quickly and went back to work!  We fell asleep so fast that night.  It took me about 10 mins to fall asleep.  The next day we had intercambios (exchanges) with the Sisters in Salama.  I was paired with Hna. Orantes, who is American but also Latina.  Her parents are from Mexico and El Salvador so she knew some Spanish from before!  It's really funny because everytime we have intercambios and I'm here at San Jeronimo, we always end up going to Cacao.  Which takes 3 hours to walk, all uphill.  However, I'm not complaining, just informing!  It's funny to see them pop into bed and fall asleep so fast at the end of the day.

With Hna. Orantes we visited Hna. Carmen a member of our branch after our studies.  Long story short, "for a learning experience" (it was really for their amusement) I was asked to butcher their chicken.  So as you can tell from the photo below, I am now a pro at butchering chickens and making chicken soup from scratch!  My comp for the day backed out last minute and worked with the vegetables instead.  After that we continued to work, work and work some more until sundown.  Then we went back home.  I think I might have worked Hna. Orantes too hard because her heels were bleeding from the walking and she was badly sunburned.  I felt really bad so I cut some aloe vera and helped bandage her up.  Poor Hna. Orantes walked all day in new non-broken in shoes!  She slept very well that night!  The next day we switched back to our original companions and continued to work.  

This week we also got the chance to help some of our members (Hna. Luz and Hna. Lily) paint and prepare their father/husband's tombs for the day of the dead.  I'm actually really excited to see how Guatemala celebrates this holiday.  It's might not be as big as it is in some other areas but I'm just as excited.

We also had a couple of miracles happen here on the mission.  Four investigators attended church today!!  One of them  named Alejandra we gave a copy of The Book of Mormon with our testimonies written inside.  During our classes after sacrament, Alejandra began to read it.  It was really beautiful because she started to cry from our testimonies and when she finished that she began reading the book.  This moment only proved to me that The Book of Mormon is true and hold the truth and evidence of the gospel.  If you have any doubts or questions or want to strengthen your testimony, start reading The Book of Mormon.  It promises in the introduction that if you read and pray with real intent, you can know for yourself that this Book is true.  That Joseph Smith was a prophet, that God is our Heavenly Father and Jesus Christ is his Son and our savior.

So yep, that was my week.  Kind of crazy, but awesome!  Thank you for reading and have a happy Halloween!

Love Hna. Possié

Fotos:
1.  Hermana Orantes and I
2.  Took this picture right after Hna. Orantes almost fell from the bridge!
3.  I for real butchered this chicken, PILAS!!
4.  She thought about it but 'CHICKENED' out last minute!
5.  Hna. Carmen, me and Hna. Orantes after enjoying chicken soup!
6.  Painting Hna. Lily's husband's tomb in preperation for dia de los muertos.
7.  It will look so pretty when everyone finishes decorating.
8.  Multizone October 2022.

<div class="gallery" data-columns="2">
    <img src="/images/posts/10-31-2022/image_01_10-31-2022.jpg">
    <img src="/images/posts/10-31-2022/image_02_10-31-2022.jpg">
    <img src="/images/posts/10-31-2022/image_03_10-31-2022.jpg">
    <img src="/images/posts/10-31-2022/image_04_10-31-2022.jpg">
    <img src="/images/posts/10-31-2022/image_05_10-31-2022.jpg">
    <img src="/images/posts/10-31-2022/image_06_10-31-2022.jpg">
    <img src="/images/posts/10-31-2022/image_07_10-31-2022.jpg">
    <img src="/images/posts/10-31-2022/image_08_10-31-2022.jpg">
</div>
