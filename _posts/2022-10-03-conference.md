---
title: "Week 11"
subtitle: 'October 3, 2022'
date: 2022-10-03 00:00:00 -700
description: "2022 Fall Conference!!"
featured_image: '/images/posts/10-03-2022/image_05_10-03-2022.jpg'
---
![](/images/posts/10-03-2022/image_05_10-03-2022.jpg)

Hola todos! I'm really excited to write to you guys this week! A lot has happened as normal.

Well, now I have a lot to explain from last week. I started my p-day by being in the Capital to fill out immigration papers. Which I was told was easy, but wasn't really. The girl that helped me probably wasn't really in a good mood so she made sure to ask me a lot of questions. The first one being "Can you speak Spanish?" My dumb brave self decided to say at that moment..."si, hablo Español." That only initiated a rapid fire of Spanish.  I was fortunate to have the gift of tongues and was able to understand and respond to her. Some of the other Elders there didn't have much luck, so I am grateful.

I also met a mother and son who were also waiting for immigration, they just happened to also be members of the church. You can always tell someone is a member when they're waving at you and greet you with open arms. Well... sometimes the creepy boys do that too but I won't get into that. The mom and son asked me to share a short scripture with them, and so I did. It was an amazing experience I had in such a short amount of time. When we finished, they thanked me and asked me how much time I have been on my mission. When I responded 4 weeks, they bursted into laughter. They thought I had been out for a year. That was a proud moment. We then traveled back to Cobán and settled down in the sisters home. I got to have a warm shower, which is always an upgrade from what we have now. Which is a bucket of cold water. If you want to have warm water you have to boil it on the stove. But I am actually used to cold showers now.

On Wednesday I spent my day with Hermana Halls, who is from Morgan UT. You don't know how excited she got when I told her that my grandparents also live in Morgan.  I am also a fan of the Tangees (did not mention they are currently shut down), and the Morgan fair is the best. We visited many members and shared many messages in Spanish. Also be proud of us, we talked to each other in Spanish the whole time. We had fun talking about things that we missed and the mission in general. She gave me words of wisdom and tips on "how to survive". She's the best!

We spent the night in Cobán and the next day had our Multi Zone conference. That was crazy! All of the hermanas dressed up with perfume, dresses sin tierra (clean dresses), and makeup. Which is normally something we go without. We had a lot of fun. It was kind of a shock factor seeing so many Elders there. We also learned something new, putting 30 Elders together calls for chaos!!  We listened to many testimonies from missionaries that are leaving, which brought many many tears. The spirit was extremely strong. I got to talk to many Elders and visit with them. Which was really weird because that was something the CCM didn't exactly allow.  All in all, I really enjoyed my first multi zone meeting. The next one will be coming really fast. I feel like time has been racing during my mission. I've changed quite a bit since I've left, and I have to remind myself it's only been 4 weeks.

Some weird foods I ate this week were cold fish soup. But that's not even the best part. Once I broke into the fish and started eating, my 5th bite in I noticed little black dots in the soup. Ants and flies. I didn't really notice before since I didn't wear my contacts that day, but I ended up finishing the bowl. Lesson learned, always bring glasses or contacts to the meals!

General Conference was this week, and it was an amazing experience. I got to watch it in Spanish and again with the aid of the gift of tongues I could understand a lot! Many people watched it at their homes, we watched at the church with 4 other people. If you haven't seen it yet, watch it and let me know who's talk was your favorite. I'm still deciding on mine, since I didn't understand some of the talks. I'll let you know soon!

Today we spent our p-day shopping since we spent our last p-day traveling to Cobán. During this week we had to get creative with some of our meals because of the lack of food. For example, one night we ate fried tomatoes. This was a thought I had, and only thanks to the movie green fried tomatoes.  I've been having some of those weird thoughts this week. For example I think I've sung 8 different Bruno Mars songs on random occasions this week. I've quoted You've Got Mail and Sleepless in Seattle probably 18 times, and then continued to laugh at myself because no one else understands. I also sang the fruit salad song and the only person I've met during my mission who knew that song was Hermana Schramm. It would be that wonderful girl! We could relate to each other in such weird ways.

Spanish is also coming along, I'm starting to take Spanish classes via zoom from an Elder. That should help a lot.
Anyways, I was just informed by Hermana Morales that this week we'll be going to the capital again for her papers. So, another crazy week! I'll let you guys know how it goes! Love you guys so much! Talk to you soon!!

Fotos:
1. Me helping Hermano Nino make a "paca" (haystack)
2. Awww..... I was actually singing Sunflower by Post Malone all day after this.
3. Ahhh no. Wasn't a chocobanano but i still ate it.
4. Hermana Morales, Elder Salvador, y yo.
5. Las Hermanas
6. Anyone want to learn Q'eqchi' with me?
7. Conference day!!
8. New Temple in Huehuetenango!!

<div class="gallery" data-columns="2">
    <img src="/images/posts/10-03-2022/image_01_10-03-2022.jpg">
    <img src="/images/posts/10-03-2022/image_02_10-03-2022.jpg">
    <img src="/images/posts/10-03-2022/image_03_10-03-2022.jpg">
    <img src="/images/posts/10-03-2022/image_04_10-03-2022.jpg">
    <img src="/images/posts/10-03-2022/image_05_10-03-2022.jpg">
    <img src="/images/posts/10-03-2022/image_06_10-03-2022.jpg">
    <img src="/images/posts/10-03-2022/image_07_10-03-2022.jpg">
    <img src="/images/posts/10-03-2022/image_08_10-03-2022.jpg">
</div>
