---
title: "Week 8"
subtitle: 'September 12, 2022'
date: 2022-09-12 00:00:00 -700
description: "Feed my sheep!"
featured_image: '/images/posts/09-12-2022/image_04_09-12-2022.jpg'
---
![](/images/posts/09-12-2022/image_04_09-12-2022.jpg)

Hola todos! Here's a rundown of my week! Lo siento (I'm sorry), i don't have much time to write today, but if you have any questions or comments, shoot me an email!

This week was a good week. I can finally teach a lesson is Spanish now!  After days and days of practicing my Spanish is coming along. This week I participated in more conversations as well, which is a big improvement for me because last week was just a smile and nod type of situation.

We had three baptisms lined up last week, however due to personal reasons, these familias had to postpone for another week. Ahh, there's always problems before baptisms aren't there!  Other than that I've been teaching a lot of lessons on the restoration and sharing passages from the living christ.

Thanks for reading. Sorry this week was so short! I'll put lots of pictures to make up for it.

Love you all! Talk to you next week!

1. Selfe with the comp....Hermana Morales!
2. My first birthday party here in Guatemala with Hermana Naty
3. My Hermana is a chef!
4. The central park
5. Mi novio!
6. One of our investigators Julian
7. Hermana Lily and Hermana Luz making popusas
8. Helado!
9. I made a popusa!
10. DINNER!  Just kidding... it was just a guest.... not dinner.

<div class="gallery" data-columns="2">
    <img src="/images/posts/09-12-2022/image_01_09-12-2022.jpg">
    <img src="/images/posts/09-12-2022/image_02_09-12-2022.jpg">
    <img src="/images/posts/09-12-2022/image_03_09-12-2022.jpg">
    <img src="/images/posts/09-12-2022/image_04_09-12-2022.jpg">
    <img src="/images/posts/09-12-2022/image_05_09-12-2022.jpg">
    <img src="/images/posts/09-12-2022/image_06_09-12-2022.jpg">
    <img src="/images/posts/09-12-2022/image_07_09-12-2022.jpg">
    <img src="/images/posts/09-12-2022/image_08_09-12-2022.jpg">
    <img src="/images/posts/09-12-2022/image_09_09-12-2022.jpg">
    <img src="/images/posts/09-12-2022/image_10_09-12-2022.jpg">
</div>
