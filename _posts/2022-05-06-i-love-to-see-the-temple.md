---
title: "I love to see the temple, I'm going there someday..."
subtitle: 'May 5, 2022'
date: 2022-05-05 00:00:00 -700
description: 'Hola todos!! Today was the day I’ve been waiting for!! My first endowment session at the Ogden temple.'
featured_image: '/images/posts/5-6-2022/2022-05-06_10-08-43_302.jpg'
---
![](/images/posts/5-6-2022/2022-05-06_10-08-43_302.jpg)

Hola todos!! Today was the day I've been waiting for!! My first endowment session at the Ogden temple. After all, my primary, Sunday school, and young woman days are coming to an end. All of the lessons teaching of temples, doesn't even compare to what it's like going through. Many people told me several times that going through the temple for the first time can be overwhelming and even confusing at some points. To tell you the truth, yes, that basically explains how I felt today. However, despite that fact, I never felt the spirit of the Holy Ghost stronger than today. The temple is so beautiful, inside and out. I can't wait till my mission receives their temple (coming September 2023)!!! To all those out there reading this, GO TO THE TEMPLE!! It's such an amazing experience, even if you're just on temple grounds. The feeling of peace and beauty that comes from the temple can fill your day with joy! Have a great semana usted!!

<div class="gallery" data-columns="4">
    <img src="/images/posts/5-6-2022/2022-05-06_10-06-59_315.jpg">
    <img src="/images/posts/5-6-2022/2022-05-06_10-08-43_302.jpg">
    <img src="/images/posts/5-6-2022/2022-05-06_10-07-53_368.jpg">
    <img src="/images/posts/5-6-2022/2022-05-06_10-08-15_891.jpg">
    <img src="/images/posts/5-6-2022/2022-05-06_10-08-28_797.jpg">
    <img src="/images/posts/5-6-2022/2022-05-06_10-08-46_570.jpg">
    <img src="/images/posts/5-6-2022/2022-05-06_10-08-51_611.jpg">
    <img src="/images/posts/5-6-2022/2022-05-06_10-09-01_013.jpg">
    <img src="/images/posts/5-6-2022/2022-05-06_10-09-07_647.jpg">
</div>
