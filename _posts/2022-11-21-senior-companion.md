---
title: "Week 18"
subtitle: 'November 21, 2022'
date: 2022-11-21 00:00:00 -700
description: "Senior Companion...."
featured_image: 'images/posts/11-21-2022/image_01_11-21-2022.jpg'
---
![](/images/posts/11-21-2022/image_01_11-21-2022.jpg)

Dear Possié Family,

It is a pleasure to greet you. We inform you that, recently, your daughter Sister
Possié has been called to serve as Senior companion in the Guatemala Cobán
mission. We have complete assurance that this call has come from the Lord.

Your missionary now has a responsibility to teach, minister, and show others how
to Find, Teach, Baptize, and Retain. Your leadership based on the Savior's example
and teachings will be the hallmark of your ministry at this stage, for with much
love and high expectations you will elevate and bless the missionaries. We invite
you to pray for her in this new stage as a leader, as well as for those she leads.

We are so grateful for raising and nurturing her child so righteous and good It is
truly a privilege to collaborate with her.

We pray that the Lord will bless you with the richest blessings of the gospel now
and always.

With appreciation,

President and Sister Alvarado

Guatemala Cobán Mission
